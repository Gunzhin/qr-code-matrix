<?php

namespace Gunzhin\QRCodeMatrix\Contracts;

interface QRCodeMatrixContract
{
    public const CODING_METHOD_DIGIT = 'D';
    public const CODING_METHOD_DIGIT_LETTER = 'DL';
    public const CODING_METHOD_BYTE = 'B';

    public const CORRECTION_LEVEL_LOW = 'L';
    public const CORRECTION_LEVEL_MEDIUM = 'M';
    public const CORRECTION_LEVEL_HIGH = 'H';

    /**
     * @return string
     */
    public function origin(): string;

    /**
     * @return string
     */
    public function codingMethod(): string;

    /**
     * @return string
     */
    public function correctionLevel(): string;

    /**
     * @return int
     */
    public function version(): int;

    /**
     * @return array[]
     */
    public function content(): array;
}
