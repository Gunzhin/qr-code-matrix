<?php

namespace Gunzhin\QRCodeMatrix\Contracts;

interface QRCodeMatrixGeneratorContract
{
    /**
     * @param string $origin
     * @param string $correction_level
     * @param string $coding_method
     *
     * @return QRCodeMatrixContract
     */
    public function make(string $origin, string $correction_level, string $coding_method): QRCodeMatrixContract;
}
