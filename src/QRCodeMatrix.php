<?php

namespace Gunzhin\QRCodeMatrix;

use Gunzhin\QRCodeMatrix\Contracts\QRCodeMatrixContract;
use Gunzhin\QRCodeMatrix\Exceptions\InvalidParametersException;
use Gunzhin\QRCodeMatrix\Exceptions\NotSupportedException;

class QRCodeMatrix implements QRCodeMatrixContract
{
    /**
     * @var string
     */
    protected $origin;

    /**
     * @var string
     */
    protected $coding_method;

    /**
     * @var string
     */
    protected $correction_level;

    /**
     * @var integer
     */
    protected $version;

    /**
     * @var array
     */
    protected $content;

    /**
     * @param string $origin
     * @param string $coding_method
     * @param string $correction_level
     * @param int $version
     * @param array $content
     *
     * @throws NotSupportedException
     * @throws InvalidParametersException
     */
    public function __construct(
        string $origin,
        string $coding_method,
        string $correction_level,
        int $version,
        array $content
    )
    {
        $this->origin = $origin;

        if ($coding_method === self::CODING_METHOD_BYTE) {
            $this->coding_method = $coding_method;
        } elseif (in_array($coding_method, self::getAvailableCodingMethods())) {
            throw new NotSupportedException($coding_method);
        } else {
            throw new InvalidParametersException($coding_method);
        }

        if (in_array($correction_level, self::getAvailableCorrectionLevels())) {
            $this->correction_level = $correction_level;
        } else {
            throw new InvalidParametersException($correction_level);
        }

        if ($version > 0 && $version <= 40) {
            $this->version = $version;
        } else {
            throw new InvalidParametersException($version);
        }

        $this->content = $content;
    }

    /**
     * @return string
     */
    public function origin(): string
    {
        return $this->origin;
    }

    /**
     * @return string
     */
    public function codingMethod(): string
    {
        return $this->coding_method;
    }

    /**
     * @return string
     */
    public function correctionLevel(): string
    {
        return $this->correction_level;
    }

    /**
     * @return int
     */
    public function version(): int
    {
        return $this->version;
    }

    /**
     * @return array
     */
    public function content(): array
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public static function getAvailableCodingMethods(): array
    {
        return [
            self::CODING_METHOD_DIGIT,
            self::CODING_METHOD_DIGIT_LETTER,
            self::CODING_METHOD_BYTE,
        ];
    }

    /**
     * @return array
     */
    public static function getAvailableCorrectionLevels(): array
    {
        return [
            self::CORRECTION_LEVEL_LOW,
            self::CORRECTION_LEVEL_MEDIUM,
            self::CORRECTION_LEVEL_HIGH,
        ];
    }
}
