<?php

namespace Gunzhin\QRCodeMatrix;

use Gunzhin\QRCodeMatrix\Contracts\QRCodeMatrixContract;
use Gunzhin\QRCodeMatrix\Contracts\QRCodeMatrixGeneratorContract;
use Gunzhin\QRCodeMatrix\Exceptions\InvalidParametersException;
use Gunzhin\QRCodeMatrix\Exceptions\NotSupportedException;

class QRCodeMatrixGenerator implements QRCodeMatrixGeneratorContract
{
    /**
     * @param string $origin
     * @param string $correction_level
     * @param string $coding_method
     *
     * @return QRCodeMatrixContract
     *
     * @throws InvalidParametersException
     * @throws NotSupportedException
     */
    public function make(
        string $origin,
        string $correction_level = QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM,
        string $coding_method = QRCodeMatrixContract::CODING_METHOD_BYTE
    ): QRCodeMatrixContract
    {
        $b_origin = $this->makeBinaryOrigin($origin, $coding_method);
        $version = $this->getVersion($b_origin, $correction_level);
        $b_service = $this->makeBinaryService($b_origin, $coding_method, $version);

        $data_numbers_blocks = $this->makeFilledNumbersBlocks($b_service . $b_origin, $correction_level, $version);
        $correction_numbers_blocks = $this->makeCorrectionBytes($data_numbers_blocks, $correction_level, $version);
        $result_binary_string = $this->makeResultBinaryString($data_numbers_blocks, $correction_numbers_blocks);

        $content = $this->makeMatrixContent($result_binary_string, $version, $correction_level);

        return new QRCodeMatrix(
            $origin,
            $coding_method,
            $correction_level,
            $version,
            $content
        );
    }

    protected function makeBinaryOrigin(string $content, string $coding_method): string
    {
        $b_origin = '';

        switch ($coding_method) {
            case QRCodeMatrixContract::CODING_METHOD_BYTE:
                for ($n = 0; $n < strlen($content); $n++) {
                    $b_origin .= str_pad(decbin(ord($content[$n])), 8, '0', STR_PAD_LEFT);
                }
                break;

            default:
                throw new NotSupportedException($coding_method);
        }

        return $b_origin;
    }

    protected function makeBinaryService(string $b_origin, string $coding_method, int $version): string
    {
        switch ($coding_method) {
            case QRCodeMatrixContract::CODING_METHOD_BYTE:
                $count_length = $version >= 10 ? 16 : 8;

                $b_type = '0100';
                $b_count = str_pad(decbin(strlen($b_origin) / 8), $count_length, '0', STR_PAD_LEFT);
                break;

            default:
                throw new NotSupportedException($coding_method);
        }

        return $b_type . $b_count;
    }

    protected function getVersionSizes(string $correction_level): array
    {
        $sizes = [
            QRCodeMatrixContract::CORRECTION_LEVEL_LOW => [
                1 => 152, 2 => 272, 3 => 440, 4 => 640, 5 => 864,
                6 => 1088, 7 => 1248, 8 => 1552, 9 => 1856, 10 => 2192,
                11 => 2592, 12 => 2960, 13 => 3424, 14 => 3688, 15 => 4184,
                16 => 4712, 17 => 5176, 18 => 5768, 19 => 6360, 20 => 6888,
                21 => 7456, 22 => 8048, 23 => 8752, 24 => 9392, 25 => 10208,
                26 => 10960, 27 => 11744, 28 => 12248, 29 => 13048, 30 => 13880,
                31 => 14744, 32 => 15640, 33 => 16568, 34 => 17528, 35 => 18448,
                36 => 19472, 37 => 20528, 38 => 21616, 39 => 22496, 40 => 23648,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM => [
                1 => 128, 2 => 224, 3 => 352, 4 => 512, 5 => 688,
                6 => 864, 7 => 992, 8 => 1232, 9 => 1456, 10 => 1728,
                11 => 2032, 12 => 2320, 13 => 2672, 14 => 2920, 15 => 3320,
                16 => 3624, 17 => 4056, 18 => 4504, 19 => 5016, 20 => 5352,
                21 => 5712, 22 => 6256, 23 => 6880, 24 => 7312, 25 => 8000,
                26 => 8496, 27 => 9024, 28 => 9544, 29 => 10136, 30 => 10984,
                31 => 11640, 32 => 12328, 33 => 13048, 34 => 13800, 35 => 14496,
                36 => 15312, 37 => 15936, 38 => 16816, 39 => 17728, 40 => 18672,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_HIGH => [
                1 => 72, 2 => 128, 3 => 208, 4 => 288, 5 => 368,
                6 => 480, 7 => 528, 8 => 688, 9 => 800, 10 => 976,
                11 => 1120, 12 => 1264, 13 => 1440, 14 => 1576, 15 => 1784,
                16 => 2024, 17 => 2264, 18 => 2504, 19 => 2728, 20 => 3080,
                21 => 3248, 22 => 3536, 23 => 3712, 24 => 4112, 25 => 4304,
                26 => 4768, 27 => 5024, 28 => 5288, 29 => 5608, 30 => 5960,
                31 => 6344, 32 => 6760, 33 => 7208, 34 => 7688, 35 => 7888,
                36 => 8432, 37 => 8768, 38 => 9136, 39 => 9776, 40 => 10208,
            ],
        ];

        return $sizes[$correction_level];
    }

    protected function getVersion(string $data, string $correction_level): int
    {
        $settings_sizes = $this->getVersionSizes($correction_level);

        $size = 0;
        $version = 0;

        $data_count = strlen($data) + 20;

        for ($v = 1; $v <= 9, $data_count > $size; $v++) {
            $size = $settings_sizes[$v];
            $version = $v;
        }

        return $version;
    }

    protected function getBlocksCount(string $correction_level): array
    {
        $groups = [
            QRCodeMatrixContract::CORRECTION_LEVEL_LOW => [
                1 => 1, 2 => 1, 3 => 1, 4 => 1, 5 => 1, 6 => 2, 7 => 2, 8 => 2, 9 => 2, 10 => 4,
                11 => 4, 12 => 4, 13 => 4, 14 => 4, 15 => 6, 16 => 6, 17 => 6, 18 => 6, 19 => 7, 20 => 8,
                21 => 8, 22 => 9, 23 => 9, 24 => 10, 25 => 12, 26 => 12, 27 => 12, 28 => 13, 29 => 14, 30 => 15,
                31 => 16, 32 => 17, 33 => 18, 34 => 19, 35 => 19, 36 => 20, 37 => 21, 38 => 22, 39 => 24, 40 => 25,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM => [
                1 => 1, 2 => 1, 3 => 1, 4 => 2, 5 => 2, 6 => 4, 7 => 4, 8 => 4, 9 => 5, 10 => 5,
                11 => 5, 12 => 8, 13 => 9, 14 => 9, 15 => 10, 16 => 10, 17 => 11, 18 => 13, 19 => 14, 20 => 16,
                21 => 17, 22 => 17, 23 => 18, 24 => 20, 25 => 21, 26 => 23, 27 => 25, 28 => 26, 29 => 28, 30 => 29,
                31 => 31, 32 => 33, 33 => 35, 34 => 37, 35 => 38, 36 => 40, 37 => 43, 38 => 45, 39 => 47, 40 => 49,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_HIGH => [
                1 => 1, 2 => 1, 3 => 2, 4 => 4, 5 => 4, 6 => 4, 7 => 5, 8 => 6, 9 => 8, 10 => 8,
                11 => 11, 12 => 11, 13 => 16, 14 => 16, 15 => 18, 16 => 16, 17 => 19, 18 => 21, 19 => 25, 20 => 25,
                21 => 25, 22 => 34, 23 => 30, 24 => 32, 25 => 35, 26 => 37, 27 => 40, 28 => 42, 29 => 45, 30 => 48,
                31 => 51, 32 => 54, 33 => 57, 34 => 60, 35 => 63, 36 => 66, 37 => 70, 38 => 74, 39 => 77, 40 => 81,
            ],
        ];

        return $groups[$correction_level];
    }

    protected function makeFilledNumbersBlocks(string $data, string $correction_level, int $version): array
    {
        $settings_groups = $this->getBlocksCount($correction_level);

        $blocks = str_split($data, 8);

        $blocks[] = str_pad(array_pop($blocks), 8, '0');

        $exists_blocks_count = count($blocks);

        $size = $this->getVersionSizes($correction_level)[$version];
        $required_blocks_count = $size / 8;

        for ($n = 0; $exists_blocks_count + $n < $required_blocks_count; $n++) {
            $blocks[] = ($n + 1) % 2
                ? '11101100'
                : '00010001';
        }

        $groups_count = $settings_groups[$version];

        $default_block_size = intdiv($required_blocks_count, $groups_count);
        $additional_parts_count = $required_blocks_count % $groups_count;

        $data_blocks = [];

        $general_data_string = implode($blocks);

        for ($nmb = 0, $ofs = 0; $nmb < $groups_count; $nmb++) {
            $additional_size = ($nmb >= $groups_count - $additional_parts_count) ? 1 : 0;
            $data_block_size = $default_block_size + $additional_size;

            $data_blocks[$nmb] = substr($general_data_string, $ofs, $data_block_size * 8);

            $ofs += $data_block_size * 8;
        }

        $result = [];

        foreach ($data_blocks as $key => $block) {
            $bytes = str_split($block, 8);

            foreach ($bytes as $byte) {
                $result[$key][] = bindec($byte);
            }
        }

        return $result;
    }

    protected function makeCorrectionBytes($data, $correction_level, $version): array
    {
        $correction_bytes_numbers = [
            QRCodeMatrixContract::CORRECTION_LEVEL_LOW => [
                1 => 7, 2 => 10, 3 => 15, 4 => 20, 5 => 26, 6 => 18, 7 => 20, 8 => 24, 9 => 30, 10 => 18,
                11 => 20, 12 => 24, 13 => 26, 14 => 30, 15 => 22, 16 => 24, 17 => 28, 18 => 30, 19 => 28, 20 => 28,
                21 => 28, 22 => 28, 23 => 30, 24 => 30, 25 => 26, 26 => 28, 27 => 30, 28 => 30, 29 => 30, 30 => 30,
                31 => 30, 32 => 30, 33 => 30, 34 => 30, 35 => 30, 36 => 30, 37 => 30, 38 => 30, 39 => 30, 40 => 30,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM => [
                1 => 10, 2 => 16, 3 => 26, 4 => 18, 5 => 24, 6 => 16, 7 => 18, 8 => 22, 9 => 22, 10 => 26,
                11 => 30, 12 => 22, 13 => 22, 14 => 24, 15 => 24, 16 => 28, 17 => 28, 18 => 26, 19 => 26, 20 => 26,
                21 => 26, 22 => 28, 23 => 28, 24 => 28, 25 => 28, 26 => 28, 27 => 28, 28 => 28, 29 => 28, 30 => 28,
                31 => 28, 32 => 28, 33 => 28, 34 => 28, 35 => 28, 36 => 28, 37 => 28, 38 => 28, 39 => 28, 40 => 28,
            ],
            QRCodeMatrixContract::CORRECTION_LEVEL_HIGH => [
                1 => 17, 2 => 28, 3 => 22, 4 => 16, 5 => 22, 6 => 28, 7 => 26, 8 => 26, 9 => 24, 10 => 28,
                11 => 24, 12 => 28, 13 => 22, 14 => 24, 15 => 24, 16 => 30, 17 => 28, 18 => 28, 19 => 26, 20 => 28,
                21 => 30, 22 => 24, 23 => 30, 24 => 30, 25 => 30, 26 => 30, 27 => 30, 28 => 30, 29 => 30, 30 => 30,
                31 => 30, 32 => 30, 33 => 30, 34 => 30, 35 => 30, 36 => 30, 37 => 30, 38 => 30, 39 => 30, 40 => 30,
            ],
        ];

        $correction_polynomials = [
            7 => [87, 229, 146, 149, 238, 102, 21],
            10 => [251, 67, 46, 61, 118, 70, 64, 94, 32, 45],
            13 => [74, 152, 176, 100, 86, 100, 106, 104, 130, 218, 206, 140, 78],
            15 => [8, 183, 61, 91, 202, 37, 51, 58, 58, 237, 140, 124, 5, 99, 105],
            16 => [120, 104, 107, 109, 102, 161, 76, 3, 91, 191, 147, 169, 182, 194, 225, 120],
            17 => [43, 139, 206, 78, 43, 239, 123, 206, 214, 147, 24, 99, 150, 39, 243, 163, 136],
            18 => [215, 234, 158, 94, 184, 97, 118, 170, 79, 187, 152, 148, 252, 179, 5, 98, 96, 153],
            20 => [17, 60, 79, 50, 61, 163, 26, 187, 202, 180, 221, 225, 83, 239, 156, 164, 212, 212, 188, 190],
            22 => [210, 171, 247, 242, 93, 230, 14, 109, 221, 53, 200, 74, 8, 172, 98, 80, 219, 134, 160, 105, 165, 231],
            24 => [229, 121, 135, 48, 211, 117, 251, 126, 159, 180, 169, 152, 192, 226, 228, 218, 111, 0, 117, 232, 87, 96, 227, 21],
            26 => [173, 125, 158, 2, 103, 182, 118, 17, 145, 201, 111, 28, 165, 53, 161, 21, 245, 142, 13, 102, 48, 227, 153, 145, 218, 70],
            28 => [168, 223, 200, 104, 224, 234, 108, 180, 110, 190, 195, 147, 205, 27, 232, 201, 21, 43, 245, 87, 42, 195, 212, 119, 242, 37, 9, 123],
            30 => [41, 173, 145, 152, 216, 31, 179, 182, 50, 48, 110, 86, 239, 96, 222, 125, 42, 173, 226, 193, 224, 130, 156, 37, 251, 216, 238, 40, 192, 180],
        ];

        $final_fields = [
            1,2,4,8,16,32,64,128,29,58,116,232,205,135,19,38,
            76,152,45,90,180,117,234,201,143,3,6,12,24,48,96,192,
            157,39,78,156,37,74,148,53,106,212,181,119,238,193,159,35,
            70,140,5,10,20,40,80,160,93,186,105,210,185,111,222,161,
            95,190,97,194,153,47,94,188,101,202,137,15,30,60,120,240,
            253,231,211,187,107,214,177,127,254,225,223,163,91,182,113,226,
            217,175,67,134,17,34,68,136,13,26,52,104,208,189,103,206,
            129,31,62,124,248,237,199,147,59,118,236,197,151,51,102,204,
            133,23,46,92,184,109,218,169,79,158,33,66,132,21,42,84,
            168,77,154,41,82,164,85,170,73,146,57,114,228,213,183,115,
            230,209,191,99,198,145,63,126,252,229,215,179,123,246,241,255,
            227,219,171,75,150,49,98,196,149,55,110,220,165,87,174,65,
            130,25,50,100,200,141,7,14,28,56,112,224,221,167,83,166,
            81,162,89,178,121,242,249,239,195,155,43,86,172,69,138,9,
            18,36,72,144,61,122,244,245,247,243,251,235,203,139,11,22,
            44,88,176,125,250,233,207,131,27,54,108,216,173,71,142,1,
        ];

        $reversed_final_fields = [
            0,0,1,25,2,50,26,198,3,223,51,238,27,104,199,75,
            4,100,224,14,52,141,239,129,28,193,105,248,200,8,76,113,
            5,138,101,47,225,36,15,33,53,147,142,218,240,18,130,69,
            29,181,194,125,106,39,249,185,201,154,9,120,77,228,114,166,
            6,191,139,98,102,221,48,253,226,152,37,179,16,145,34,136,
            54,208,148,206,143,150,219,189,241,210,19,92,131,56,70,64,
            30,66,182,163,195,72,126,110,107,58,40,84,250,133,186,61,
            202,94,155,159,10,21,121,43,78,212,229,172,115,243,167,87,
            7,112,192,247,140,128,99,13,103,74,222,237,49,197,254,24,
            227,165,153,119,38,184,180,124,17,68,146,217,35,32,137,46,
            55,63,209,91,149,188,207,205,144,135,151,178,220,252,190,97,
            242,86,211,171,20,42,93,158,132,60,57,83,71,109,65,162,
            31,45,67,216,183,123,164,118,196,23,73,236,127,12,111,246,
            108,161,59,82,41,157,85,170,251,96,134,177,187,204,62,90,
            203,89,95,176,156,169,160,81,11,245,22,235,122,117,44,215,
            79,174,213,233,230,231,173,232,116,214,244,234,168,80,88,175,
        ];

        $correction_bytes_number = $correction_bytes_numbers[$correction_level][$version];

        $null_filled_block = array_fill(0, $correction_bytes_number, 0);

        $correction_polynomial = $correction_polynomials[$correction_bytes_number];

        $correction_blocks = [];

        foreach ($data as $origin_block) {
            $block_count = count($origin_block);

            $filled_block = array_replace($null_filled_block, $origin_block);

            $correction_block = [];

            for ($n = 0; $n < $block_count; $n++) {
                $value = array_shift($filled_block);
                $filled_block[] = 0;

                if ($value > 0) {
                    $reversed_final_value = $reversed_final_fields[$value];

                    foreach ($correction_polynomial as $key => $correction_polynomial_number) {
                        $increased_correction_polynomial_number = $correction_polynomial_number + $reversed_final_value;

                        if ($increased_correction_polynomial_number >= 255) {
                            $increased_correction_polynomial_number %= 255;
                        }

                        $filled_block[$key] = $filled_block[$key] ^ $final_fields[$increased_correction_polynomial_number];
                    }

                    $correction_block = $filled_block;
                }
            }

            $correction_blocks[] = $correction_block;
        }

        return $correction_blocks;
    }

    protected function makeResultBinaryString($data_blocks, $correction_blocks): string
    {
        $result_binary_string = '';

        foreach ([$data_blocks, $correction_blocks] as $blocks) {
            for ($n = 0, $c = true; $c; $n++) {
                $c = false;
                foreach ($blocks as $block) {
                    if (array_key_exists($n, $block)) {
                        $binary_value = decbin($block[$n]);
                        $normalized_binary_value = str_pad($binary_value, 8, '0', STR_PAD_LEFT);

                        $result_binary_string .= $normalized_binary_value;
                        $c = true;
                    }
                }
            }
        }

        return $result_binary_string;
    }

    protected function makeMatrixContent(string $binary_string, int $version, string $correction_level): array
    {
        $qr_code_size = 17 + ($version * 4) + 8;

        $leveling_pixels_centers_versions = [
            1 => [],
            2 => [18],
            3 => [22],
            4 => [26],
            5 => [30],
            6 => [34],
            7 => [6, 22, 38],
            8 => [6, 24, 42],
            9 => [6, 26, 46],
            10 => [6, 28, 50],
            11 => [6, 30, 54],
            12 => [6, 32, 58],
            13 => [6, 34, 62],
            14 => [6, 26, 46, 66],
            15 => [6, 26, 48, 70],
            16 => [6, 26, 50, 74],
            17 => [6, 30, 54, 78],
            18 => [6, 30, 56, 82],
            19 => [6, 30, 58, 86],
            20 => [6, 34, 62, 90],
            21 => [6, 28, 50, 72, 94],
            22 => [6, 26, 50, 74, 98],
            23 => [6, 30, 54, 78, 102],
            24 => [6, 28, 54, 80, 106],
            25 => [6, 32, 58, 84, 110],
            26 => [6, 30, 58, 86, 114],
            27 => [6, 34, 62, 90, 118],
            28 => [6, 26, 50, 74, 98, 122],
            29 => [6, 30, 54, 78, 102, 126],
            30 => [6, 26, 52, 78, 104, 130],
            31 => [6, 30, 56, 82, 108, 134],
            32 => [6, 34, 60, 86, 112, 138],
            33 => [6, 30, 58, 86, 114, 142],
            34 => [6, 34, 62, 90, 118, 146],
            35 => [6, 30, 54, 78, 102, 126, 150],
            36 => [6, 24, 50, 76, 102, 128, 154],
            37 => [6, 28, 54, 80, 106, 132, 158],
            38 => [6, 32, 58, 84, 110, 136, 162],
            39 => [6, 26, 54, 82, 110, 138, 166],
            40 => [6, 30, 58, 86, 114, 142, 170],
        ];

        $leveling_pixels_centers = $leveling_pixels_centers_versions[$version];

        $content = [];

        for ($n = 0; $n < $qr_code_size; $n++) {
            $content[$n] = array_fill(0, $qr_code_size, -1);
        }

        for ($x = 0; $x < $qr_code_size; $x++) {
            for ($y = 0; $y < 4; $y++) {
                $content[$x][$y] = 0;
                $content[$x][$qr_code_size - $y - 1] = 0;
                $content[$y][$x] = 0;
                $content[$qr_code_size - $y - 1][$x] = 0;
            }
        }

        $search_squares = [
            [4, 4],
            [4, $qr_code_size - 11],
            [$qr_code_size - 11, 4],
        ];

        foreach ($search_squares as $coordinates) {
            $start_x = $coordinates[0];
            $start_y = $coordinates[1];

            for ($n = 0; $n < 7; $n++) {
                $content[$start_x + $n][$start_y + 0] = 1;
                $content[$start_x + $n][$start_y + 6] = 1;
                $content[$start_x + 0][$start_y + $n] = 1;
                $content[$start_x + 6][$start_y + $n] = 1;
            }

            for ($n = -1; $n < 8; $n++) {
                $content[$start_x + $n][$start_y - 1] = 0;
                $content[$start_x + $n][$start_y + 7] = 0;
                $content[$start_x - 1][$start_y + $n] = 0;
                $content[$start_x + 7][$start_y + $n] = 0;
            }

            for ($n = 1; $n < 6; $n++) {
                $content[$start_x + $n][$start_y + 1] = 0;
                $content[$start_x + $n][$start_y + 5] = 0;
                $content[$start_x + 1][$start_y + $n] = 0;
                $content[$start_x + 5][$start_y + $n] = 0;
            }

            for ($n = 0; $n < 3; $n++) {
                for ($i = 0; $i < 3; $i++) {
                    $content[$start_x + $n + 2][$start_y + $i + 2] = 1;
                }
            }
        }

        $sync_line_start = [12, 10];

        for ($n = 0; $n < $qr_code_size - 22; $n++) {
            $x = $n + $sync_line_start[0];
            $y = $sync_line_start[1];

            $content[$x][$y] = ($n + 1) % 2;
            $content[$y][$x] = ($n + 1) % 2;
        }

        if (! empty($leveling_pixels_centers)) {
            foreach ($leveling_pixels_centers as $key_x => $leveling_x) {
                $leveling_x += 4;
                foreach ($leveling_pixels_centers as $key_y => $leveling_y) {
                    $leveling_y += 4;

                    if (
                        $leveling_x + $leveling_y > 20 &&
                        abs($leveling_x - $leveling_y) + 21 !== $qr_code_size
                    ) {
                        for ($n = -2; $n < 3; $n++) {
                            $content[$leveling_x + $n][$leveling_y - 2] = 1;
                            $content[$leveling_x + $n][$leveling_y + 2] = 1;
                            $content[$leveling_x - 2][$leveling_y + $n] = 1;
                            $content[$leveling_x + 2][$leveling_y + $n] = 1;
                        }

                        for ($n = -1; $n < 2; $n++) {
                            $content[$leveling_x + $n][$leveling_y - 1] = 0;
                            $content[$leveling_x + $n][$leveling_y + 0] = 0;
                            $content[$leveling_x + $n][$leveling_y + 1] = 0;
                        }

                        $content[$leveling_x][$leveling_y] = 1;
                    }
                }
            }
        }

        $versions_matrix = [
            7 =>  [[0,0,0,0,1,0],[0,1,1,1,1,0],[1,0,0,1,1,0]],
            8 =>  [[0,1,0,0,0,1],[0,1,1,1,0,0],[1,1,1,0,0,0]],
            9 =>  [[1,1,0,1,1,1],[0,1,1,0,0,0],[0,0,0,1,0,0]],
            10 => [[1,0,1,0,0,1],[1,1,1,1,1,0],[0,0,0,0,0,0]],
            11 => [[0,0,1,1,1,1],[1,1,1,0,1,0],[1,1,1,1,0,0]],
            12 => [[0,0,1,1,0,1],[1,0,0,1,0,0],[0,1,1,0,1,0]],
            13 => [[1,0,1,0,1,1],[1,0,0,0,0,0],[1,0,0,1,1,0]],
            14 => [[1,1,0,1,0,1],[0,0,0,1,1,0],[1,0,0,0,1,0]],
            15 => [[0,1,0,0,1,1],[0,0,0,0,1,0],[0,1,1,1,1,0]],
            16 => [[0,1,1,1,0,0],[0,1,0,0,0,1],[0,1,1,1,0,0]],
            17 => [[1,1,1,0,1,0],[0,1,0,1,0,1],[1,0,0,0,0,0]],
            18 => [[1,0,0,1,0,0],[1,1,0,0,1,1],[1,0,0,1,0,0]],
            19 => [[0,0,0,0,1,0],[1,1,0,1,1,1],[0,1,1,0,0,0]],
            20 => [[0,0,0,0,0,0],[1,0,1,0,0,1],[1,1,1,1,1,0]],
            21 => [[1,0,0,1,1,0],[1,0,1,1,0,1],[0,0,0,0,1,0]],
            22 => [[1,1,1,0,0,0],[0,0,1,0,1,1],[0,0,0,1,1,0]],
            23 => [[0,1,1,1,1,0],[0,0,1,1,1,1],[1,1,1,0,1,0]],
            24 => [[0,0,1,1,0,1],[0,0,1,1,0,1],[1,0,0,1,0,0]],
            25 => [[1,0,1,0,1,1],[0,0,1,0,0,1],[0,1,1,0,0,0]],
            26 => [[1,1,0,1,0,1],[1,0,1,1,1,1],[0,1,1,1,0,0]],
            27 => [[0,1,0,0,1,1],[1,0,1,0,1,1],[1,0,0,0,0,0]],
            28 => [[0,1,0,0,0,1],[1,1,0,1,0,1],[0,0,0,1,1,0]],
            29 => [[1,1,0,1,1,1],[1,1,0,0,0,1],[1,1,1,0,1,0]],
            30 => [[1,0,1,0,0,1],[0,1,0,1,1,1],[1,1,1,1,1,0]],
            31 => [[0,0,1,1,1,1],[0,1,0,0,1,1],[0,0,0,0,1,0]],
            32 => [[1,0,1,0,0,0],[0,1,1,0,0,0],[1,0,1,1,0,1]],
            33 => [[0,0,1,1,1,0],[0,1,1,1,0,0],[0,1,0,0,0,1]],
            34 => [[0,1,0,0,0,0],[1,1,1,0,1,0],[0,1,0,1,0,1]],
            35 => [[1,1,0,1,1,0],[1,1,1,1,1,0],[1,0,1,0,0,1]],
            36 => [[1,1,0,1,0,0],[1,0,0,0,0,0],[0,0,1,1,1,1]],
            37 => [[0,1,0,0,1,0],[1,0,0,1,0,0],[1,1,0,0,1,1]],
            38 => [[0,0,1,1,0,0],[0,0,0,0,1,0],[1,1,0,1,1,1]],
            39 => [[1,0,1,0,1,0],[0,0,0,1,1,0],[0,0,1,0,1,1]],
            40 => [[1,1,1,0,0,1],[0,0,0,1,0,0],[0,1,0,1,0,1]],
        ];

        $offset_x = 4;
        $offset_y = $qr_code_size - 15;

        if ($version >= 7) {
            $version_matrix = $versions_matrix[$version];

            foreach ($version_matrix as $y => $line_x) {
                foreach ($line_x as $x => $value) {
                    $content[$x + $offset_x][$y + $offset_y] = $value;
                    $content[$y + $offset_y][$x + $offset_x] = $value;
                }
            }
        }

        $mask_correction_codes = [
            QRCodeMatrixContract::CORRECTION_LEVEL_LOW => [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
            QRCodeMatrixContract::CORRECTION_LEVEL_MEDIUM => [1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0],
            QRCodeMatrixContract::CORRECTION_LEVEL_HIGH => [0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1],
        ];

        $mask_correction_code = $mask_correction_codes[$correction_level];

        for ($n = 0; $n < 7; $n++) {
            $x1 = $n + 4;
            $y1 = 12;

            $x2 = 12;
            $y2 = $qr_code_size - $x1 - 1;

            if ($n === 6) {
                $x1++;
                $content[$x2][$y2 - 1] = 1;
            }

            $content[$x1][$y1] = $mask_correction_code[$n];
            $content[$x2][$y2] = $mask_correction_code[$n];
        }

        for ($n = 0; $n < 8; $n++) {
            $x1 = 12;
            $y1 = 12 - $n;

            $x2 = $qr_code_size - $y1;
            $y2 = 12;

            if ($n >= 2) {
                $y1--;
            }

            $content[$x1][$y1] = $mask_correction_code[$n + 7];
            $content[$x2][$y2] = $mask_correction_code[$n + 7];
        }

        $x = $qr_code_size - 5;
        $y = $qr_code_size - 5;
        $dx = -1;
        $dy = -1;
        $dp = -1;

        $content_order = [
            [$x, $y]
        ];

        while ($x > 3) {
            if ($dp > 0) {
                $y += $dy;
            }

            $x += $dx;

            $dx *= -1;
            $dp *= -1;

            if (
                $y < 4 ||
                $y > $qr_code_size - 4
            ) {
                $dy *= -1;

                $y += $dy;
                $x -= ($x === 12) ? 3 : 2;
            }

            $value = $content[$x][$y] ?? 0;

            if ($value < 0) {
                $content_order[] = [$x, $y];
            }
        }

        for ($n = 0; $n < strlen($binary_string); $n++) {
            $origin_value = (int) $binary_string[$n];

            if (array_key_exists($n, $content_order)) {
                [$x, $y] = $content_order[$n];

                $mask_value = ($x - 4) % 3 === 0
                    ? (int) !$origin_value
                    : $origin_value;

                $content[$x][$y] = $mask_value;
            }
        }

        return $content;
    }
}
